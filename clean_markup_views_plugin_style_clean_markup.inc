<?php
/**
 * @file
 * Contains the responsive grid style plugin.
 */

/**
 * Style plugin to render each item in a responsive grid.
 *
 * @ingroup views_style_plugins
 */
class clean_markup_views_plugin_style_clean_markup extends views_plugin_style {
  /**
   * Set default options.
   */
  function option_definition() {
    $options = parent::option_definition();
    
    // $options['wrapper'] = array('default' => 'none');
    // $options['wrapper_id'] = array('default' => '');
    // $options['wrapper_classes'] = array('default' => '');
    // $options['wrapper_style'] = array('default' => '');

    // $options['rows'] = array('default' => 'none');
    // $options['rows_classes'] = array('default' => '');

    // $options['row'] = array('default' => 'none');
    // $options['row_id'] = array('default' => '');
    // $options['row_classes'] = array('default' => '');
    // $options['row_style'] = array('default' => '');

    return $options;
  }

  /**
   * Render the given style.
   */
  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);
    
    // Flatten options to deal with the various hierarchy changes.
    $options = clean_markup_views_get_options($this->options);

    $form['rows_wrapper'] = array(
      '#type' => 'select',
      '#title' => t('Rows wrapper markup'),
      '#description' => t('Choose the HTML element to wrap the rows.'),
      '#default_value' => $options['rows_wrapper'],
      '#options' => clean_markup_views_get_html_wrappers_options(),
    );
    $form['add_rows_wrapper_classes'] = array(
      '#type' => 'textfield',
      '#title' => t('Additional rows wrapper classes'),
      '#description' => t('Additional classes to set on the rows wrapper.'),
      '#default_value' => $options['add_rows_wrapper_classes'],
      '#states' => array(
        'invisible' => array(
          ':input[name="style_options[rows_wrapper]"]' => array('value' => CLEAN_MARKUP_NO_ELEMENT),
        ),
      ),
    );
    $form['add_rows_wrapper_attributes'] = array(
      '#type' => 'textfield',
      '#title' => t('Additional attributes'),
      '#description' => t('Additional attributes to set on the rows wrapper (i.e. <code>role="navigation"</code>). Text entered here will not be sanitized.') . '<br />' .
      t('While this is a powerful and flexible feature if used by a trusted user with HTML experience, it is a security risk in the hands of a malicious or inexperienced user.'),
      '#default_value' => $options['add_rows_wrapper_attributes'],
      '#states' => array(
        'invisible' => array(
          ':input[name="style_options[rows_wrapper]"]' => array('value' => CLEAN_MARKUP_NO_ELEMENT),
        ),
      ),
    );

    $form['rows_wrapper_inner_div'] = array(
      '#type' => 'checkbox',
      '#title' => t('Enable inner div'),
      '#description' => t('Specify if you want an inner div element inside the main view wrapper.'),
      '#default_value' => $options['view_wrapper_enable_inner_div'],
      '#states' => array(
        'invisible' => array(
          ':input[name="style_options[rows_wrapper]"]' => array('value' => CLEAN_MARKUP_NO_ELEMENT),
        ),
      ),
    );

    $form['rows_wrapper_inner_div_classes'] = array(
      '#type' => 'textfield',
      '#title' => t('Classes for rows inner div'),
      '#description' => t('Classes to set on the rows wrapper inner div.'),
      '#default_value' => $options['rows_wrapper_inner_div_classes'],
      '#states' => array(
        'invisible' => array(
          ':input[name="style_options[rows_wrapper]"]' => array('value' => CLEAN_MARKUP_NO_ELEMENT),
        ),
      ),
    );

    $form['row_wrapper'] = array(
      '#type' => 'select',
      '#title' => t('Row wrapper markup'),
      '#description' => t('Choose the HTML element to wrap each row.'),
      '#default_value' => $options['row_wrapper'],
      '#options' => clean_markup_views_get_html_wrappers_options(),
    );
    
    $form['add_row_wrapper_classes'] = array(
      '#type' => 'textfield',
      '#title' => t('Additional row wrapper classes'),
      '#description' => t('Additional classes to set on the row wrapper.'),
      '#default_value' => $options['add_row_wrapper_classes'],
      '#states' => array(
        'invisible' => array(
          ':input[name="style_options[row_wrapper]"]' => array('value' => CLEAN_MARKUP_NO_ELEMENT),
        ),
      ),
    );

    $form['first_row_wrapper_classes'] = array(
      '#type' => 'textfield',
      '#title' => t('Additional row wrapper classes for first row'),
      '#description' => t('Additional classes to set on the first row wrapper.'),
      '#default_value' => $options['first_row_wrapper_classes'],
      '#states' => array(
        'invisible' => array(
          ':input[name="style_options[row_wrapper]"]' => array('value' => CLEAN_MARKUP_NO_ELEMENT),
        ),
      ),
    );

    $form['last_row_wrapper_classes'] = array(
      '#type' => 'textfield',
      '#title' => t('Additional row wrapper classes for last row'),
      '#description' => t('Additional classes to set on the last row wrapper.'),
      '#default_value' => $options['last_row_wrapper_classes'],
      '#states' => array(
        'invisible' => array(
          ':input[name="style_options[row_wrapper]"]' => array('value' => CLEAN_MARKUP_NO_ELEMENT),
        ),
      ),
    );
    
    $form['add_row_wrapper_attributes'] = array(
      '#type' => 'textfield',
      '#title' => t('Additional attributes'),
      '#description' => t('Additional attributes to set on the row wrapper (i.e. <code>role="navigation"</code>). Text entered here will not be sanitized.') . '<br />' .
      t('While this is a powerful and flexible feature if used by a trusted user with HTML experience, it is a security risk in the hands of a malicious or inexperienced user.'),
      '#default_value' => $options['add_row_wrapper_attributes'],
      '#states' => array(
        'invisible' => array(
          ':input[name="style_options[row_wrapper]"]' => array('value' => CLEAN_MARKUP_NO_ELEMENT),
        ),
      ),
    );
  }
}
