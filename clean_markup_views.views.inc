<?php
/**
 * @file
 * Views-specific implementations and functions.
 */

/**
 * Implements hook_views_plugins().
 */
function clean_markup_views_views_plugins() {
  return array(
    'style' => array(
      'clean_markup' => array(
        'title' => t('Clean markup'),
        'help' => t('Display content in a clean views markup.'),
        'handler' => 'clean_markup_views_plugin_style_clean_markup',
        'theme' => 'views_view_clean',
        'uses row plugin' => TRUE,
        'uses row class' => FALSE,
        'uses options' => TRUE,
        'uses grouping' => TRUE,
        'type' => 'normal',
      ),
    ),
  );
}
