<?php if (!empty($title)) : ?>
  <h3><?php print $title; ?></h3>
<?php endif; ?>
<?php if ($display_rows_wrapper): ?><<?php print $rows_wrapper; ?><?php print $add_rows_wrapper_attributes; ?> class="<?php print $add_rows_wrapper_classes; ?>"><?php endif; ?>
  <?php if ($rows_wrapper_inner_div_classes): ?><div class="<?php print $rows_wrapper_inner_div_classes; ?>"><?php endif; ?>
    <?php foreach ($rows as $id => $row): ?>
      <?php if ($display_row_wrapper): ?><<?php print $row_wrapper; ?><?php print $add_row_wrapper_attributes[$id]; ?> class="<?php print $add_row_wrapper_classes[$id]; ?>"><?php endif; ?>
        <?php print $row; ?>
      <?php if ($display_row_wrapper): ?></<?php print $row_wrapper; ?>><?php endif; ?>
    <?php endforeach; ?>
  <?php if ($rows_wrapper_inner_div_classes): ?></div><?php endif; ?>
<?php if ($display_rows_wrapper): ?></<?php print $rows_wrapper; ?>><?php endif; ?>
